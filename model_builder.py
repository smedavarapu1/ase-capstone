from utils import data_Load
from my_CNN_model import *
import cv2


X_train, y_train = data_Load()

my_model = CNNArch()

compileModel(my_model, optimizer = 'adam', loss = 'mean_squared_error', metrics = ['accuracy'])


hist = trainModel(my_model, X_train, y_train)

saveModel(my_model, 'my_model')



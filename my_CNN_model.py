from keras.models import Sequential
from keras.models import load_model
from keras.layers import Convolution2D, MaxPooling2D, Dropout
from keras.layers import Flatten, Dense
from keras.optimizers import SGD, RMSprop, Adagrad, Adadelta, Adam, Adamax, Nadam

def CNNArch():

    m = Sequential()
    m.add(Convolution2D(32, (5, 5), input_shape=(96,96,1), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))

    m.add(Convolution2D(64, (3, 3), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))
    m.add(Dropout(0.1))

    m.add(Convolution2D(128, (3, 3), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))
    m.add(Dropout(0.2))

    m.add(Convolution2D(512, (3, 3), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))
    m.add(Dropout(0.2))

    m.add(Convolution2D(4096, (3, 3), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))
    m.add(Dropout(0.2))

    m.add(Convolution2D(30, (3, 3), activation='relu'))
    m.add(MaxPooling2D(pool_size=(2, 2)))
    m.add(Dropout(0.3))

    m.add(Flatten())

    m.add(Dense(64, activation='relu'))
    m.add(Dense(128, activation='relu'))
    m.add(Dense(256, activation='relu'))
    m.add(Dense(64, activation='relu'))
    m.add(Dense(30))

    return m;

def compileModel(m, optimizer, loss, metrics):
    m.compile(optimizer=optimizer, loss=loss, metrics=metrics)

def trainModel(m, X_train, y_train):
    return m.fit(X_train, y_train, epochs=100, batch_size=200, verbose=1, validation_split=0.2)

def saveModel(m, fileName):
    m.save(fileName + '.h5')

def loadModel(fileName):
    return load_model(fileName + '.h5')

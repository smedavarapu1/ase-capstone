import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

from keras.models import load_model
from pandas.io.parsers import read_csv
from sklearn.utils import shuffle

def data_Load(test=False):
   
    trainF = 'data/training.csv'
    testF = 'data/test.csv'
    nameFrame = testF if test else trainF
    dataFRAME = read_csv(os.path.expanduser(nameFrame))
    dataFRAME['Image'] = dataFRAME['Image'].apply(lambda im: np.fromstring(im, sep=' '))

    dataFRAME = dataFRAME.dropna() 

    B = np.vstack(dataFRAME['Image'].values) / 255. 
    B = B.astype(np.float32)
    B = B.reshape(-1, 96, 96, 1) 

    if not test: 
        C = dataFRAME[dataFRAME.columns[:-1]].values
        C = (C - 48) / 48 
        B, C = shuffle(B, C, random_state=42) 
        C = C.astype(np.float32)
    else:
        C = None

    return B, C
